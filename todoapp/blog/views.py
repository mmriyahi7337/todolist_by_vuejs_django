from django.http import Http404
from django.shortcuts import render
from .models import *
from django.views.generic import ListView, DetailView


# Create your views here.

class NewsList(ListView):
    model = news
    context_object_name = 'newsList'


class NewsDetail(DetailView):
    model = news
    template_name = "blog/newsInner.html"
    context_object_name = 'instance'
