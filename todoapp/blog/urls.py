from django.urls import path
from .views import *

app_name = 'blog'

urlpatterns = [
    path('', NewsList.as_view(), name="home"),
    path('news/<int:pk>', NewsDetail.as_view(), name="newsdetail"),

]
