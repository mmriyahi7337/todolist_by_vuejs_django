from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, View
from django.contrib.auth.views import LoginView, LogoutView
from todo.models import Task
from todo.forms import TaskForm
from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.shortcuts import render, reverse
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import RegisterForm


# Create your views here.


class Login(LoginView):
    pass


class register(CreateView):
    form_class = RegisterForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('todo:login')


class Logout(LoginRequiredMixin, LogoutView):
    pass


class TaskView(LoginRequiredMixin, View):
    def get(self, request):
        tasks = list(Task.objects.filter(owner=self.request.user).values())
        if request.is_ajax():
            return JsonResponse({'tasks': tasks}, status=200)

        return render(request, 'task/task.html')

    def post(self, request):
        bound_form = TaskForm(request.POST)

        if bound_form.is_valid():
            bound_form.instance.owner = request.user
            new_task = bound_form.save()
            return JsonResponse({'task': model_to_dict(new_task)}, status=200)

        return redirect('tasklist')


class TaskComplete(LoginRequiredMixin, View):
    def post(self, request, id):
        task = Task.objects.get(id=id)
        task.completed = True
        task.save()
        return JsonResponse({'task': model_to_dict(task)}, status=200)


class TaskDelete(LoginRequiredMixin, View):
    def post(self, request, id):
        task = Task.objects.get(id=id)
        task.delete()
        return JsonResponse({'result': 'ok'}, status=200)
