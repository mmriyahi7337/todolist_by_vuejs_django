from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic import View, ListView, CreateView
from django.contrib.auth.views import LoginView, LogoutView
from django.http import JsonResponse
from django.forms.models import model_to_dict
