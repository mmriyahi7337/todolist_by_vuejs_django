from django.contrib import admin
from .models import *

# Register your models here.
admin.site.site_header = 'Todo list Project'


class taskadmin(admin.ModelAdmin):
    list_display = ('title', 'date')
    list_filter = (['owner'])
    search_fields = ('title', 'owner')


admin.site.register(Task, taskadmin)
