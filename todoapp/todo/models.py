from django.db import models
from account.models import User


# Create your models here.
class Task(models.Model):
    title = models.CharField(max_length=140)
    date = models.DateTimeField(auto_now_add=True)
    completed = models.BooleanField(default=False)
    owner = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name='tasks')

    class Meta:
        ordering = ['completed', 'date']
