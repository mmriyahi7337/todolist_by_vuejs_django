from django.urls import path
from .views import *
from account import views as acv

app_name = 'todo'
urlpatterns = [
    path('', acv.TaskView.as_view(), name='tasklist'),
    path('<str:id>/complete/', acv.TaskComplete.as_view()),
    path('<str:id>/delete/', acv.TaskDelete.as_view()),
    path('login/', acv.Login.as_view(), name="login"),
    path('logout/', acv.Logout.as_view(), name="logout"),
    path('register/', acv.register.as_view(), name="register"),
]
